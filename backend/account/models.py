from django.db import models
from django.core.validators import validate_email
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils import timezone

class CustomUserManager(BaseUserManager):
    def _create_user(self, email, firstname, lastname, roles, is_staff, is_active, password,
    **extra_fields):

        if not email:
            raise ValueError('The given email must be set')

        email = self.normalize_email(email)
        user = self.model(  email=email,
                            firstname=firstname,
                            lastname=lastname,
                            roles=roles,
                            is_staff=is_staff,
                            is_active=is_active,
                            **extra_fields)
        user.set_password(password)
        user.save(using=self.db)

        return user

    def create_user(self,  email, firstname, lastname, roles, is_staff, is_active ,password=None, **extra_fields):
        return self._create_user(email, firstname, lastname, roles, is_staff, is_active, password, **extra_fields)

    def create_superuser(self, email, firstname, lastname, roles, is_staff, is_active, password=None, **extra_fields):
        return self._create_user(email, firstname, lastname, "Super User",  True, True, password,  **extra_fields)

class CustomUser(AbstractBaseUser, PermissionsMixin):
    created_at = models.DateTimeField(default=timezone.now)
    email = models.EmailField(max_length=254, unique=True, validators=[validate_email])
    firstname = models.CharField(max_length=100, default='')
    lastname = models.CharField(max_length=100, default='')
    STATUS_USER = (
        ("Super User", "Super User"),
        ("Admin", "Admin"), 
        ("Member", "Member"),
    )
    roles = models.CharField(max_length=50, choices=STATUS_USER, default='Member')
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['firstname', 'lastname', 'roles', 'is_staff', 'is_active',]

    objects = CustomUserManager()

    def save(self, *args, **kwargs):
        if self.roles == 'Supper User':
            self.is_staff = True

        self.email = self.email.lower()
        return super().save(*args, **kwargs)

    def __str__(self):
        return '{} / {}'.format(self.email, self.firstname)
