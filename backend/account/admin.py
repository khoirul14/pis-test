from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import CustomUser

class CustomUserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ['email', 'password', 'firstname', 'lastname', 'roles']}),
        ('Permissions', {'fields': ['is_active', 'is_staff', 'is_superuser']}),
    )
    add_fieldsets = (
        (None, {
            'classes': ['wide'],
            'fields':['email', 'firstname', 'lastname', 'roles', 'is_active', 'password1', 'password2']
        }),
    )
    list_display = ['email', 'firstname', 'lastname', 'roles', 'is_active']

    list_filter = ['roles']
    search_fields = ['email','firstname', 'lastname', 'roles']
    ordering = ['roles', 'email', 'firstname', 'lastname']


admin.site.site_header = "PIS Administrator"
admin.site.register(CustomUser, CustomUserAdmin)
