from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView, TemplateView
from .models import CustomUser
from .forms import CustomUserForm, CustomUserChangeForm
from django.db.models import Q
from django.urls import reverse_lazy, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from api import models
from django.core.mail import EmailMessage
from backend import settings
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.encoding import force_bytes, force_text, DjangoUnicodeDecodeError
from django.contrib.auth.hashers import make_password
from .utils import account_activation_token
# Create your views here.

def login_request(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                # messages.info(request, "Logout Successful.")
                return redirect('/')
            else:
                messages.error(request, "Your Email Address and Password didn't match. Please try again!")
        else:
            messages.error(request, "Your Email Address and Password didn't match. Please try again!")
    form = AuthenticationForm()
    return render(request = request, template_name = "registration/login.html", context={"form":form})

class Register(View):
    def get(self, request, *args, **kwargs):
        return render(request,'registration/register.html')

    def post(self, request, *args, **kwargs):
        emailuser = request.POST['email']
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        password1 = request.POST['password1']
        password2 = request.POST['password2']

        if not CustomUser.objects.filter(email=emailuser).exists():
            if password1 and password2 and password1 != password2:
                messages.error(request, "Password didn't match")
                return render(request, 'registration/register.html')

            elif len(password1) < 3:
                messages.error(
                        request, 'Password too short. Password must be more 3 character'
                    )
                return render(request, 'registration/register.html')

            user = CustomUser.objects.create_user(
                    email=emailuser, firstname=firstname, lastname=lastname,
                    roles='Member', is_active=False, is_staff=False
                )
            user.set_password(password1)
            user.save()

            ''' send email activation account '''
            uidb64 = urlsafe_base64_encode(force_bytes(user.pk))

            domain = get_current_site(request).domain
            link = reverse('activate', kwargs={
                                'uidb64': uidb64, 
                                'token': account_activation_token.make_token(user)
                            })
            
            activate_url = 'http://' + domain + link
            email_subject = 'Activate your account'
            email_body = 'Hi ' + user.firstname + \
                        ' Please use this link to verify your account\n ' + activate_url
            # email_body = activate_url
            email = EmailMessage(
                email_subject,
                email_body,
                'noreply@i-gen.co.id',
                [emailuser],
            )
            email.send(fail_silently=False)

            messages.success(request, 'Account successfuly created')
            return render(request, 'registration/confirm-mail.html', {
                'emailuser': emailuser,
            })

        return render(request,'registration/register.html')
    

class VerificationView(View):
    def get(self, request, uidb64, token):

        try: 
            id = force_bytes(urlsafe_base64_decode(uidb64))
            user = CustomUser.objects.get(pk=id)

            if not account_activation_token.check_token(user, token):
                return redirect('login-user'+'?message='+'User already activated')

            if user.is_active:
                print('masuk sini')
                return redirect('login-user')
            user.is_active = True
            user.save()

            messages.success(request, 'Account activated successfully')
            return redirect('login-user')

        except Exception as ex:
            pass

        return redirect('login-user')