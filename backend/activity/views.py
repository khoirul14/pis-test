from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView, TemplateView
from api import models
from . import forms
from django.db.models import Q
from django.urls import reverse_lazy, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
# Create your views here.

class MainDashboard(LoginRequiredMixin, TemplateView):
    def get(self, request, *args, **kwargs):
        all_age = models.Customer.objects.all().order_by('age')

        xLabel = []
        yLabel = []

        for x in range(1, (10+1)):
            income_per_age = 0
            customer_per_age = models.Customer.objects.filter(age__range = [ (((x-1)*10)+1) , (x*10) ])
            for cs in customer_per_age:
                income_per_age = income_per_age + cs.income
            
            xLabel.append(str(((x-1)*10)+1) + ' s/d '+ str(x*10))
            yLabel.append(income_per_age)

        return render(request, 'dashboard/main/index.html', {
            'xLabel': xLabel,
            'yLabel': yLabel,
            'main_dashboard_page_active': 'current',
        })


class CustomerListView(LoginRequiredMixin, ListView):
    model = models.Customer
    context_object_name = 'customer_list'
    template_name = 'dashboard/customer/list.html'

    def get_queryset(self):
        pencarian = self.model.objects.all()
        query = self.request.GET.get('q')
        if query:
            query_list = pencarian.filter(
                Q(firstname__startswith = query) |
                Q(middlename__startswith = query) |
                Q(lastname__startswith = query) | 
                Q(address__icontains = query) |
                Q(phone_number__contains = query) |
                Q(status__contains = query) |
                Q(age__contains = query) |
                Q(income__icontains = query)
            )
        else:
            query_list = pencarian
        return query_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['customer_page_active'] = 'current'
        return context

class CustomerCreateView(LoginRequiredMixin,CreateView):
    model = models.Customer
    template_name = 'dashboard/customer/create.html'
    form_class = forms.CustomerForm
    success_url = reverse_lazy('customer-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['customer_page_active'] = 'current'
        return context

class CustomerUpdateView(LoginRequiredMixin,UpdateView):
    model = models.Customer
    template_name = 'dashboard/customer/update.html'
    form_class = forms.CustomerForm
    success_url = reverse_lazy('customer-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['customer_page_active'] = 'current'
        return context

class CustomerDeleteView(LoginRequiredMixin,DeleteView):
    model = models.Customer
    template_name = 'dashboard/customer/delete.html'
    success_url = reverse_lazy('customer-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['customer_page_active'] = 'current'
        return context
