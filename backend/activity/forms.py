from django import forms
from api import models

class CustomerForm(forms.ModelForm):

    class Meta:
        model = models.Customer
        fields = '__all__'
        exclude = ['created_at']