from django.urls import path
from . import views

urlpatterns = [
    path('', views.MainDashboard.as_view(), name='main-dashboard'),

    path('dashboard/customer', views.CustomerListView.as_view(), name='customer-list'),
    path('dashboard/customer/create', views.CustomerCreateView.as_view(), name='customer-create'),
    path('dashboard/customer/update/<int:pk>', views.CustomerUpdateView.as_view(), name='customer-update'),
    path('dashboard/customer/delete/<int:pk>', views.CustomerDeleteView.as_view(), name='customer-delete'),
]
