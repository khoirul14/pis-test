"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.contrib.auth import views
from django.conf.urls.static import static
from account import views as account_views

urlpatterns = [
    path('admin/', admin.site.urls),

    # activity dashboard
    path('', include('activity.urls')),

    path('register', account_views.Register.as_view(), name='register-user'),
    path('login', account_views.login_request, name='login-user'),
    path('logout', views.LogoutView.as_view(next_page='/'), name='logout-user'),
    path('activate/<uidb64>/<token>', account_views.VerificationView.as_view(), name='activate')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
