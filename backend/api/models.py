from django.db import models
from django.utils import timezone

# Create your models here.

class Customer(models.Model):
    created_at = models.DateTimeField(default=timezone.now)
    firstname = models.CharField(max_length=150, default='')
    middlename = models.CharField(max_length=150, default='')
    lastname = models.CharField(max_length=150, default='')
    address = models.TextField(default='')
    company_name = models.CharField(max_length=255, default='')
    phone_number = models.CharField(max_length=20, default='')
    STATUS_LIST = (
        ('Single', 'Single'),
        ('Married', 'Married'),
    )
    status = models.CharField(max_length=20, choices=STATUS_LIST, default='Single')
    age = models.IntegerField(default='')
    income = models.BigIntegerField(default='')